// Ionic Starter App

  // Initialize Firebase
  var config = {
    apiKey: "AIzaSyBN01prte7Nqx-EBEo0XKyu1-DYKXhOOD0",
    authDomain: "zipcheckin.firebaseapp.com",
    databaseURL: "https://zipcheckin.firebaseio.com",
    storageBucket: "zipcheckin.appspot.com",
    messagingSenderId: "1009207310227"
  };
  firebase.initializeApp(config);


// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var fb = null;
angular.module('starter', ['ionic','ngCordova','starter.controllers','firebase','ngCordovaOauth'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })
   .state('app.intro', {
    url: '/intro',
    views: {
      'menuContent': {
        templateUrl: 'templates/intro.html',
        controller: 'introCtrl'
      }
    }
  })
   .state('app.home', {
    url: '/home',
    views: {
      'menuContent': {
        templateUrl: 'templates/home.html'
      }
    }
  })
   .state('app.logoff', {
    url: '/logoff',
    views: {
      'menuContent': {
        controller: 'logoffCtrl'
      }
    }
  })
 .state('app.login', {
    url: '/login',
    templateUrl: 'templates/login.html',
    controller: 'AuthCtrl'
  })
 .state('app.profile', {
    url: '/profile',
    views: {
      'menuContent': {
        templateUrl: 'templates/profile.html',
        controller: 'ProfileCtrl'
      }
    }
  })
 .state('app.zip', {
    url: '/zip/:hotel_id',
    views: {
      'menuContent': {
        templateUrl: 'templates/zip.html',
        controller: 'ZipCtrl'
      }
    }
  })
  .state('app.map', {
    url: '/map',
    views: {
      'menuContent': {
        templateUrl: 'templates/maps.html',
        controller: 'MapController'
      }
    }
  })
  .state('app.search', {
    url: '/search',
    views: {
      'menuContent': {
        templateUrl: 'templates/search.html'
      }
    }
  })

  .state('app.browse', {
      url: '/browse',
      views: {
        'menuContent': {
          templateUrl: 'templates/browse.html',
          controller: 'docsCtrl'
        }
      }
    })
    .state('app.contato', {
      url: '/contato',
      views: {
        'menuContent': {
          templateUrl: 'templates/contato.html',
          controller: 'ContatoCtrl'
        }
      }
    })

  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/intro');
});
