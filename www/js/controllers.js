angular.module('starter.controllers', [])
.factory('GoogleMapsService', ['$rootScope', '$ionicLoading', '$timeout', '$window', '$document', 'ConnectivityService', function($rootScope, $ionicLoading, $timeout, $window, $document, ConnectivityService) {

  var apiKey = 'AIzaSyAQQ4DPq7o1nVMjJb_3zgOddz79AsiFZDQ',
    map = null,
    mapDiv = null,
    directionsService,
    directionsDisplay,
    routeResponse;

  function initService(mapEl, key) {
    mapDiv = mapEl;
    if (typeof key !== "undefined") {
      apiKey = key;
    }
    if (typeof google == "undefined" || typeof google.maps == "undefined") {
      disableMap();
      if (ConnectivityService.isOnline()) {
        $timeout(function() {
          loadGoogleMaps();
        }, 0);
      }
    } else {
      if (ConnectivityService.isOnline()) {
        initMap();
        enableMap();
      } else {
        disableMap();
      }
    }
  }

  function initMap() {
    if (mapDiv) {
      var mapOptions = {
        zoom: 10,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };
      map = new google.maps.Map(mapDiv, mapOptions);
      directionsService = new google.maps.DirectionsService();
      directionsDisplay = new google.maps.DirectionsRenderer();
      directionsDisplay.setMap(map);
      // Wait until the map is loaded
      google.maps.event.addListenerOnce(map, 'idle', function() {
        enableMap();
      });
    }
  }

  function enableMap() {
    // For demonstration purposes we’ll use a $rootScope variable to enable/disable the map.
    // However, an alternative option would be to broadcast an event and handle it in the controller.
    $rootScope.enableMap = true;
  }

  function disableMap() {
    $rootScope.enableMap = false;
  }

  function loadGoogleMaps() {
    // This function will be called once the SDK has been loaded
    $window.mapInit = function() {
      initMap();
    };

    // Create a script element to insert into the page
    var script = $document[0].createElement("script");
    script.type = "text/javascript";
    script.id = "googleMaps";

    // Note the callback function in the URL is the one we created above
    if (apiKey) {
      script.src = 'https://maps.google.com/maps/api/js?key=' + apiKey + '&callback=mapInit';
    } else {
      script.src = 'https://maps.google.com/maps/api/js?callback=mapInit';
    }
    $document[0].body.appendChild(script);
  }

  function checkLoaded() {
    if (typeof google == "undefined" || typeof google.maps == "undefined") {
      $timeout(function() {
        loadGoogleMaps();
      }, 2000);
    } else {
      enableMap();
    }
  }

  function addRoute(origin, destination, waypts, optimizeWaypts) {
    routeResponse = null;
    if (typeof google !== "undefined") {
      var routeRequest = {
        origin: origin,
        destination: destination,
        waypoints: waypts,
        optimizeWaypoints: optimizeWaypts,
        travelMode: google.maps.TravelMode.DRIVING
      };

      directionsService.route(routeRequest, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
          google.maps.event.trigger(map, 'resize');
          // Save the response so we access it from controller
          routeResponse = response;
          // Broadcast event so controller can process the route response
          $rootScope.$broadcast('googleRouteCallbackComplete');
        }
      });
    }
  }

  function removeRoute() {
    if (typeof google !== "undefined" && typeof directionsDisplay !== "undefined") {
      directionsDisplay.setMap(null);
      directionsDisplay = null;
      directionsDisplay = new google.maps.DirectionsRenderer();
      directionsDisplay.setMap(map);
    }
  }

  return {
    initService: function(mapEl, key) {
      initService(mapEl, key);
    },
    checkLoaded: function() {
      checkLoaded();
    },
    disableMap: function() {
      disableMap();
    },
    removeRoute: function() {
      removeRoute();
    },
    getRouteResponse: function() {
      return routeResponse;
    },
    addRoute: function(origin, destination, waypts, optimizeWaypts) {
      addRoute(origin, destination, waypts, optimizeWaypts);
    }
  };

}])

/*
===========================================================================
  C O N N E C T I V I T Y
===========================================================================
*/
.factory('ConnectivityService', [function() {
    return {
      isOnline: function() {
        var status = localStorage.getItem('networkStatus');
        if (status === null || status == "online") {
          return true;
        } else {
          return false;
        }
      }
    };
  }])
  /*
  ===========================================================================
    N E T W O R K
  ===========================================================================
  */
  .factory('NetworkService', ['GoogleMapsService', function(GoogleMapsService) {
    /*
     * handles network events (online/offline)
     */
    return {
      networkEvent: function(status) {
        var pastStatus = localStorage.getItem('networkStatus');
        if (status == "online" && pastStatus != status) {
          // The app has regained connectivity...
          GoogleMapsService.checkLoaded();
        }
        if (status == "offline" && pastStatus != status) {
          // The app has lost connectivity...
          GoogleMapsService.disableMap();
        }
        localStorage.setItem('networkStatus', status);
        return true;
      }
    };
  }])

.controller('AppCtrl', function($scope,$rootScope,  $state , $ionicModal, $timeout, $firebaseAuth, $cordovaOauth , $http) {

  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $rootScope.user = {};
  $rootScope.user.nome = localStorage.getItem('nome_logado');
  $rootScope.user.pic =localStorage.getItem('pic_logado');
  console.log($rootScope.user);
  var initial_state = true;
  $rootScope.isVisible = initial_state;

  // Form data for the login modal
  $scope.loginData = {};
  $scope.userlogged = {};

/*
  if(localStorage['id_logado']  != null && localStorage['id_logado']  != ""){
      $state.go("app.map");
  }
*/
  // Create the login modal that we will use later
  $ionicModal.fromTemplateUrl('templates/login.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.modal = modal;
  });

  // Triggered in the login modal to close it
  $scope.closeLogin = function() {
    $scope.modal.hide();
  };

  // Open the login modal
  $scope.login = function() {
    $scope.modal.show();
  };




$scope.displayData = function(access_token) {
    $http.get("https://graph.facebook.com/v2.2/me", {params: {access_token: access_token, fields: "name,gender,location,picture", format: "json" }}).then(function(result) {
        console.log(result);
        var name = result.data.name;
        $scope.user.name = result.data.name;
        $scope.user.id = result.data.id;
        localStorage.setItem('id_logado', result.data.id);
        localStorage.setItem('nome_logado', result.data.name);
        localStorage.setItem('pic_logado', result.data.picture.data.url);
       // $rootScope.user_id = result.data.id;
        var gender = result.data.gender;
        $scope.user.gender = result.data.gender;
        var picture = result.data.picture;
        $scope.user.picture = result.data.picture.data.url;
        $scope.userlogged = result.data;


        var html = '<table id="table" data-role="table" data-mode="column" class="ui-responsive"><thead><tr><th>Field</th><th>Info</th></tr></thead><tbody>';
        html = html + "<tr><td>" + "Name" + "</td><td>" + name + "</td></tr>";
        html = html + "<tr><td>" + "Gender" + "</td><td>" + gender + "</td></tr>";
        html = html + "<tr><td>" + "Picture" + "</td><td><img src='" + picture.data.url + "' /></td></tr>";

        html = html + "</tbody></table>";

        document.getElementById("listTable").innerHTML = html;
       // $.mobile.changePage($("#profile"), "slide", true, true);
    }, function(error) {
        alert("Error: " + error);
    });
}

  // Perform the login action when the user submits the login form
  $scope.doLogin = function() {
    console.log('Doing login', $scope.loginData);
    var fb_app_id = '329647170729203'

    //var ref = new Firebase("http://zipcheckin.firebaseio.com/");
    var auth = $firebaseAuth();
    var provider = new firebase.auth.FacebookAuthProvider();
    provider.addScope('public_profile');
    console.log(auth);
    $cordovaOauth.facebook(fb_app_id, ["email"]).then(function(result) {
    console.log(result);
    var credentials = firebase.auth.FacebookAuthProvider.credential(result.access_token);
    $scope.displayData(result.access_token);

    firebase.auth().signInWithCredential(credentials).then(function(authData) {
           // $rootScope.userlogged = authData;
            console.log("logado redirecionando: " );
            var fbAuth = auth.$getAuth();
            console.log(fbAuth);
            localStorage.setItem("fb_uid",fbAuth.uid);
            console.log( authData);
            $state.go("app.home");
        }).catch(function(error) {
            console.error("ERROR: " + error);
        });
    //$http.get("http://localhost/firetoken/?id="+result.access_token).then(function(token) {
    //console.log(token); 
    //});
  });
  }
})
.controller('introCtrl', function($scope, $state,$ionicSideMenuDelegate) {
  $scope.$watch(function () {
            return $ionicSideMenuDelegate.getOpenRatio();
        },
        function (ratio) {
          if (ratio != 0) {
            $ionicSideMenuDelegate.toggleRight();
          }
        });

    $ionicSideMenuDelegate.canDragContent(false);
    if(localStorage['id_logado']  === null){
      $state.go("app.map");
    }
})
.controller('homeCtrl', function($scope,$rootScope, $state) {
  alert("home controller");
     var feeds = {
        "US": 298,
        "SA": 200,
        "DE": 220,
        "FR": 540,
        "CN": 120,
        "AU": 760,
        "BR": 550,
        "IN": 200,
        "GB": 120
    };

    $scope.feeds = feeds;
    
})
.controller('logoffCtrl', function($scope,$rootScope, $state) {
   localStorage.clear();
   $rootScope.isVisible = false;
   localStorage['id_logado'] =''; 
   localStorage['nome_logado'] =''; 
   localStorage['pic_logado'] =''; 
   $state.go("app.intro");
    
})
.controller('ProfileCtrl', function($scope,$firebaseArray) {
    var id_logado = localStorage.getItem('id_logado');
    //var id_logado = 1662377170741570;
    var ref = firebase.database().ref().child("users/"+id_logado+"/infos");
    var infos = $firebaseArray(ref);
    $scope.infos = infos;

    $scope.fire_endereco = {};
    var ratingRef = firebase.database().ref("users/"+id_logado+"/endereco");

    var pso2 , temp , pulse;
    ratingRef.on("value", function(data) {
     data.forEach(function(data) {
        var obj = data.val();
        $("#"+obj.id).val(obj.valor);
      });
    });

     $scope.nova_info = function() {
        $scope.inserir_nova = true;
     };

     $scope.salva_info = function() {
      
      var data={"nome": $('#nome_info').val(), "valor": $('#valor_info').val(),"data": new Date(), "id_usuario": id_logado};
      //document.write(JSON.stringify(data));
        jQuery.ajax({
            accept: "application/json",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "https://zipcheckin.firebaseio.com/users/"+id_logado+"/infos.json",
            data: JSON.stringify(data),
        });
        $('#nome_info').val('');
        $('#valor_info').val('');
        
        $scope.inserir_nova = false;
     };

      function getEndereco() {  
               if($.trim($("#cep").val()) != ""){  
                   $.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep="+$("#cep").val(), function(){  
                       if(resultadoCEP["resultado"]){  
                           $("#rua").val(unescape(resultadoCEP["tipo_logradouro"])+" "+unescape(resultadoCEP["logradouro"])); 
                           $("#bairro").val(unescape(resultadoCEP["bairro"]));  
                           $("#cidade").val(unescape(resultadoCEP["cidade"]));
                           $("#uf").val(unescape(resultadoCEP["uf"]));  
                       }else{  
                           alert("Cep invalido !");  
                 //jqDialog.notify("Cep Invalido", 0);
                       }  
                   });  
               }  
       }  
     
      $('#cep').on('blur', function() {
              getEndereco();
      });
     
      $scope.salva_endereco = function() {
      
      var data={"nome": $('#nome_info').val(), "valor": $('#valor_info').val(),"data": new Date(), "id_usuario": id_logado};
      //document.write(JSON.stringify(data));
        /*jQuery.ajax({
            accept: "application/json",
            type: 'POST',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            url: "https://zipcheckin.firebaseio.com/users/"+id_logado+"/endereco.json",
            data: JSON.stringify(data),
        });
        $('#nome_info').val('');
        $('#valor_info').val('');
        */

        firebase.database().ref("users/"+id_logado+"/endereco/").remove();
        firebase.database().ref("users/"+id_logado+"/endereco/rua").set({"id":"rua","valor":$('#rua').val()});
        firebase.database().ref("users/"+id_logado+"/endereco/numero").set({"id":"numero","valor":$('#numero').val()});
        firebase.database().ref("users/"+id_logado+"/endereco/cep").set({"id":"cep","valor":$('#cep').val()});
        firebase.database().ref("users/"+id_logado+"/endereco/bairro").set({"id":"bairro","valor":$('#bairro').val()});
        firebase.database().ref("users/"+id_logado+"/endereco/cidade").set({"id":"cidade","valor":$('#cidade').val()});
        firebase.database().ref("users/"+id_logado+"/endereco/uf").set({"id":"uf","valor":$('#uf').val()});
        $scope.inserir_nova = false;
     };

})
.controller('ZipCtrl', function($scope, $stateParams , $firebaseArray, $ionicPopup, $cordovaEmailComposer) {
    var id_logado = localStorage.getItem('id_logado');
    var ref = firebase.database().ref().child("users/"+id_logado+"/infos");
    var infos = $firebaseArray(ref);
    $scope.infos = infos;
    console.log(infos);

    var refdocumentos = firebase.database().ref().child("users/"+id_logado+"/documentos");
    var documentos = $firebaseArray(refdocumentos);
    $scope.documentos = documentos;
    console.log(documentos);

    var refenderecos = firebase.database().ref().child("users/"+id_logado+"/endereco");
    var enderecos = $firebaseArray(refenderecos);
    $scope.enderecos = enderecos;
    console.log(enderecos);



     console.log($stateParams.hotel_id);
      var hoteis = JSON.parse(localStorage.getItem('hoteis_buscado'));
      for (var i = 0; i < hoteis.length; i++) {
        if($stateParams.hotel_id == hoteis[i].factual_id){
          console.log(hoteis[i]);
          $scope.hotel_selecionado = hoteis[i];
        }
      }
    $scope.tap = function() {


    if($scope.hotel_selecionado.email == undefined){
      var promptPopup = $ionicPopup.prompt({
        title: 'Sem Email cadastrado para esse hotel',
        template: 'preencha um email para enviar essas infos'
     });

     promptPopup.then(function(res) {
        if (res) {
           console.log('enviando para ', res);
           $cordovaEmailComposer.isAvailable().then(function() {
             // is available
           }, function () {
             // not available
           });

            var email = {
              to: 'danielmmf+android@gmail.com',
              cc: 'danielmmf+androidcc@gmail.com',
              bcc: ['john@doe.com', 'jane@doe.com'],
              attachments: [
                'file://img/logo.png',
                'res://icon.png',
                'base64:icon.png//iVBORw0KGgoAAAANSUhEUg...',
                'file://README.pdf'
              ],
              subject: 'Cordova Icons',
              body: 'How are you? Nice greetings from Leipzig',
              isHtml: true
            };

           $cordovaEmailComposer.open(email).then(null, function () {
             // user cancelled email
           });
        } else {
           console.log('Cancelado');
        }
     });
    }else{
      var alertPopup = $ionicPopup.alert({
        title: 'Tap !!!',
        template: "Informações enviadas para "+$scope.hotel_selecionado.email,
     });
     alertPopup.then(function(res) {
        console.log('Thanks');
     });
    }

    }
})
.controller('MapController', function($scope,$rootScope, $timeout , $ionicLoading, $cordovaGeolocation, $http,replace_dashFilter,GoogleMapsService) {
        $rootScope.isVisible = true;
        $ionicLoading.show({
            template: '<ion-spinner icon="bubbles"></ion-spinner><br/>Acquiring location!'
        });
         
        var posOptions = {
            enableHighAccuracy: true,
            timeout: 20000,
            maximumAge: 0
        };
 
        $cordovaGeolocation.getCurrentPosition(posOptions).then(function (position) {
            var lat  = position.coords.latitude;
            var long = position.coords.longitude;
            console.log('navigator ok :'+lat+' '+long);
             
            var myLatlng = new google.maps.LatLng(lat, long);

            //https://api.foursquare.com/v2/venues/search?ll=-23.9499983,-46.7&categoryId=4bf58dd8d48988d1fa931735&oauth_token=FBJSODMBQ3FS4OQATLVQ5CJK5RVNBMYAIQSEHVKOIGZDUASH&v=20161021
            //$http.get('https://api.foursquare.com/v2/venues/search?ll='+lat+','+long+'&categoryId=4bf58dd8d48988d1fa931735&oauth_token=KHW04J1D343BGPL40GO2JYNJKCNXYYGSPR5OMR3C1LJIQMTW&v=20161021').then(function(data){     
            // $http.get('https://api.foursquare.com/v2/venues/search?ll='+lat+','+long+'&categoryId=4bf58dd8d48988d1fa931735&oauth_token=FBJSODMBQ3FS4OQATLVQ5CJK5RVNBMYAIQSEHVKOIGZDUASH&v=20161021').then(function(data){     
            $http.get('http://api.v3.factual.com/t/places?geo={"$circle":{"$center":['+lat+','+long+'],"$meters":5000}}&filters={"$and":[{"category_labels":{"$includes":"hotel"}}]}&KEY=p6QSvOZawnUnTQKveAPWfS2jsuquWqq87Pp3oHGb').then(function(data){     
            //$rootScope.CurrentLocation=data.data.results[0].formatted_address;//you get the current location here
              
            localStorage.setItem('hoteis_buscado' , JSON.stringify(data.data.response.data));

            console.log(data.data.response.data);
            $scope.hoteis = data.data.response.data;

        }, function(err) {
          // error
          console.log(err);
        });

        var mapOptions = {
            center: myLatlng,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };          
         
        var map = new google.maps.Map(document.getElementById("map"), mapOptions);          
         
        $scope.map = map;   

        $scope.marker = new google.maps.Marker({
            position: myLatlng,
            map: $scope.map,
            title: 'Holas!'
        }, function(err) {
            console.err(err);
        });


        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var icons = {
          parking: {
            icon: iconBase + 'parking_lot_maps.png'
          },
          library: {
            icon: iconBase + 'library_maps.png'
          },
          info: {
            icon: iconBase + 'info-i_maps.png'
          }
        };

        function addMarker(feature) {
          var marker = new google.maps.Marker({
            position: feature.position,
            icon: icons[feature.type].icon,
            map: map
          });
        }

        var features = [
          {
            position: new google.maps.LatLng(lat, long),
            type: 'info'
          }, {
            position: new google.maps.LatLng(lat-0.000010, long-0.00012),
            type: 'info'
          }, {
            position: new google.maps.LatLng(lat-0.000020, long-0.000030),
            type: 'info'
          }
        ];

        for (var i = 0, feature; feature = features[i]; i++) {
          addMarker(feature);
        }

  //$scope.init = function () {
    GoogleMapsService.initService(document.getElementById("map"));
  //};
  
  $scope.journeyLegs = [];
  var journeyLeg = {
    "zipPostalCode": "05359190",
    "contactId": "1"
  };
  $scope.journeyLegs.push(journeyLeg);

  // Call this method to add the route
  $scope.addRoute = function(origin, destination) {
    if (origin !== "" && destination !== "") {
      // Callout to Google to get route between first and last contact.
      // The 'legs' between these contact will give us the distance/time
      var waypts = [];
      for (i = 0, len = $scope.journeyLegs.length; i < len; i++) {
        waypts.push({
          location: $scope.journeyLegs[i].zipPostalCode,
          stopover: true
        });
      }
      GoogleMapsService.addRoute(origin, destination, waypts, true);
    }
  };

  // Handle callback from Google Maps after route has been generated
  var deregisterUpdateDistance = $rootScope.$on('googleRouteCallbackComplete', function(event, args) {
    $scope.updateDistance();
  });

  // We'll add the route after the initService has loaded the Google Maps javascript
  // We're only adding this timeout here because we've called the initService in the code above, and we need to give it time to load the js
  $timeout(function() {
    // Call the method to add the route
    $scope.addRoute("16400100", "05359190");
  },3000);
    
  // Deregister the handler when scope is destroyed
  $scope.$on('$destroy', function() {
    deregisterUpdateDistance();
  });

  $scope.troca_rota = function(){
    var y = window.prompt("primeiro cep");
    var x = window.prompt("segundo cep");
   // window.alert(y)
    $scope.addRoute( y, x);
  }

  $scope.updateDistance = function() {
    $timeout(function() {
      // Get the route saved after callback from Google directionsService (to get route)
      var routeResponse = GoogleMapsService.getRouteResponse();
      if (routeResponse) {
        // We’ve only defined one route with waypoints (or legs) along it
        var route = routeResponse.routes[0];
        // The following is an example of getting the distance and duration for the last leg of the route
        var distance = route.legs[route.legs.length - 1].distance;
        var duration = route.legs[route.legs.length - 1].duration;
        console.log("distance.value = ", distance.value);
        console.log("duration.value = ", duration.value);
        console.log("duration.text = ", duration.text);
      }
    });
  };


            $ionicLoading.hide();           
             
        }, function(err) {
            $ionicLoading.hide();
            console.log(err);
        });
    
    
 
})

.controller('ContatoCtrl', function($scope, $stateParams) {

})
.controller('AuthCtrl', function($scope, $rootScope, $firebaseAuth, $cordovaOauth, $state){
  var fb_app_id = '329647170729203';
  var ref = new Firebase("http://zipcheckin.firebaseio.com/");
  var auth = $firebaseAuth(ref);
  $rootScope.user = null;

  $scope.login = function() {
    $cordovaOauth.facebook(fb_app_id, ["email"]).then(function(result) {
        auth.$authWithOAuthToken("facebook", result.access_token).then(function(authData) {
            console.log(JSON.stringify(authData));

            $rootScope.user = authData;
            $rootScope.$apply();
            $state.go('app.browse');

        }, function(error) {
            console.error("ERROR: " + error);
        });
    }, function(error) {
        console.log("ERROR: " + error);
    });
  };
})
.controller('docsCtrl', function($scope,$rootScope, $cordovaCamera ,$firebaseAuth, $ionicHistory, $firebaseArray, $cordovaCamera, $state, $firebase) {

    $ionicHistory.clearHistory();

    $scope.images = [];
    var id_logado = localStorage.getItem('id_logado');
    console.log(firebase);
    var ref = firebase.database().ref().child("users/"+id_logado+"/documentos");
    
    // create a synchronized array
    // click on `index.html` above to see it used in the DOM!
    var images = $firebaseArray(ref);
    var inputs = [];
    $scope.images = images;
    console.log(images);


     $scope.camera = function() {
        var options = {
          quality: 50,
          destinationType: Camera.DestinationType.DATA_URL,
          sourceType: Camera.PictureSourceType.CAMERA,
          allowEdit: true,
          encodingType: Camera.EncodingType.JPEG,
          targetWidth: 100,
          targetHeight: 100,
          popoverOptions: CameraPopoverOptions,
          saveToPhotoAlbum: false,
        correctOrientation:true
        };

        $cordovaCamera.getPicture(options).then(function(imageData) {
          var image = document.getElementById('myImage');
          image.src = "data:image/jpeg;base64," + imageData;
          $rootScope.arquivo_selecionado = true;
          $("#file").val(image.src);
        }, function(err) {
          // error
      });
    }
   // camera();

     function createFileEntry(fileURI) {
      window.resolveLocalFileSystemURL(fileURI, copyFile, fail);
    }

    function fail(error) {
      console.error("fail: " + error.code);
    }
 
    function makeid() {
      var text = "";
      var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
 
      for (var i=0; i < 5; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
      }
      return text;
    }

    function onCopySuccess(entry) {
      $scope.$apply(function () {
        $scope.images.push(entry.nativeURL);
      });
    }

    $scope.urlForImage = function(imageName) {
      var name = imageName.substr(imageName.lastIndexOf('/') + 1);
      var trueOrigin = cordova.file.dataDirectory + name;
      return trueOrigin;
    }
 
    // 5
    function copyFile(fileEntry) {
      var name = fileEntry.fullPath.substr(fileEntry.fullPath.lastIndexOf('/') + 1);
      console.log(name);
      var newName = makeid() + name;
 
      window.resolveLocalFileSystemURL(cordova.file.dataDirectory, function(fileSystem2) {
       

        fileEntry.copyTo(
          fileSystem2,
          newName,
          onCopySuccess,
          fail
        );

        console.log(fileSystem2);
        return name;
      },
      fail);
    }

    $scope.imagem = function() {

      var options = {
        quality: 50,
        destinationType: Camera.DestinationType.DATA_URL,
        sourceType: Camera.PictureSourceType.SAVEDPHOTOALBUM,
        targetWidth: 300,
        targetHeight: 300
      };


      $cordovaCamera.getPicture(options).then(function(imageURI) {

        //console.log(imageURI);
        var image = document.getElementById('myImage');
        //var storageRef = firebase.storage().ref();
        //console.log(storageRef);
       /* syncArray.$add({image: imageData}).then(function() {
                alert("Image has been uploaded");
        });*/
        //var image_info = createFileEntry(imageURI);
        //console.log(image_info);
        //var uploadTask = storageRef.child('images/' + imageURI).put(file[0]);
       // var uploadTask = storageRef.child('images/' + imageURI).put(imageURI);

        // Create a reference to 'mountains.jpg'
        //var mountainsRef = storageRef.child(imageURI).put(imageURI);
        image.src = imageURI;
        $rootScope.arquivo_selecionado = true;
        image.src = "data:image/jpeg;base64," + imageURI;
        $("#file").val(image.src);
        $("#bin").val(imageURI);
      }, function(err) {
        // error
        console.log(err);
      });


     
    }


    $scope.salvar_documento = function() {
      var id_logado = localStorage.getItem('id_logado');
      $rootScope.arquivo_selecionado = false;
      var data={"nome": $('#nome').val(), "bin": $('#bin').val(),"imagem": $('#file').val(),"data": new Date(), "id_usuario": id_logado};
      //document.write(JSON.stringify(data));
      jQuery.ajax({
          accept: "application/json",
          type: 'POST',
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          url: "https://zipcheckin.firebaseio.com/users/"+id_logado+"/documentos.json",
          data: JSON.stringify(data),
      });
      $('#nome').val('');
      $('#bin').val('');
      $('#file').val('');
      var image = document.getElementById('myImage');
      image.src = '';
    }
    //imagem();
}).filter('replace_dash', function () {
  return function (input) {
    return input.split('-').join('_');
  };
});
